#encoding: utf-8
class Admin::GroupsController < Admin::ApplicationController
  before_filter :group, only: [:edit, :show, :update, :destroy, :project_update, :project_teams_update]

  def index
    @groups = Group.order('name ASC')
    @groups = @groups.search(params[:name]) if params[:name].present?
    @groups = @groups.page(params[:page]).per(20)
  end

  def show
    @members = @group.members.order("access_level DESC").page(params[:members_page]).per(30)
    @projects = @group.projects.page(params[:projects_page]).per(30)
  end

  def new
    @group = Group.new
  end

  def edit
  end

  def create
    @group = Group.new(group_params)
    @group.name = @group.path.dup unless @group.name

    if @group.save
      @group.add_owner(current_user)
      redirect_to [:admin, @group], notice: '群组创建成功。'
    else
      render "new"
    end
  end

  def update
    if @group.update_attributes(group_params)
      redirect_to [:admin, @group], notice: '群组更新成功。'
    else
      render "edit"
    end
  end

  def project_teams_update
    @group.add_users(params[:user_ids].split(','), params[:access_level])

    redirect_to [:admin, @group], notice: '用户增加成功。'
  end

  def destroy
    @group.destroy

    redirect_to admin_groups_path, notice: '群组删除成功。'
  end

  private

  def group
    @group = Group.find_by(path: params[:id])
  end

  def group_params
    params.require(:group).permit(:name, :description, :path, :avatar)
  end
end
