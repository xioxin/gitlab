#encoding: utf-8
module SortingHelper
  def sort_title_oldest_updated
    '最旧更新的'
  end

  def sort_title_recently_updated
    '最近更新的'
  end

  def sort_title_oldest_created
    '最旧创建的'
  end

  def sort_title_recently_created
    '最近创建的'
  end
end
